package com.finistmart.profile.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.profile.HttpClient;
import com.finistmart.profile.portlet.cache.CacheFetcher;
import com.finistmart.profile.portlet.cache.CacheHandler;
import com.finistmart.profile.portlet.dto.Order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class MyOrdersService {

    private static final Logger logger = LoggerFactory.getLogger(MyOrdersService.class);

    private final CacheHandler<String, List<Order>> cacheHandler = new CacheHandler<>(new CacheFetcher<>());
    private final CacheHandler<String, Order> orderCacheHandler = new CacheHandler<>(new CacheFetcher<>());

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    public List<Order> fetchAllByAppIdent(HttpClient client, Long showCaseId, String appIdent) {

        List<Order> orders = new ArrayList<>();
        String requestUrl = "/api/orders";
        if (showCaseId != null) {
            requestUrl += "?showcase_id=" + String.valueOf(showCaseId) + "&app_ident=" + appIdent;
        } else {
            if (appIdent != null) {
                requestUrl += "?app_ident=" + appIdent;
            }
        }
        try {
            String responseBody = client.request(requestUrl, "GET");
            orders = mapper().readValue(responseBody, new TypeReference<List<Order>>() { });
        } catch (IOException e) {
            logger.error("Error getting orders list", e);
        }

        return orders;
    }

    public List<Order> getAllByAppIdent(HttpClient client, Long showCaseId, String appIdent) {

        List<Order> orders = cacheHandler.getEntityFromCache("orders")
                .orFromSource(() -> {
                    return this.fetchAllByAppIdent(client, showCaseId, appIdent);
                }).usingCacheKey(showCaseId != null
                        ? String.valueOf(showCaseId) + "_" + String.valueOf(appIdent) : String.valueOf(appIdent))
                ;

        return orders;
    }

}
