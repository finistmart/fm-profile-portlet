package com.finistmart.profile.portlet.dto;

import java.util.List;

public class ProductsResult {

    private Long total;
    private Integer page;
    private Integer pageSize;
    private List<Product> results;

    public ProductsResult() {
    }

    public ProductsResult(List<Product> results, Long total, Integer page, Integer pageSize) {
        this.total = total;
        this.page = page;
        this.pageSize = pageSize;
        this.results = results;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<Product> getResults() {
        return results;
    }

    public void setResults(List<Product> results) {
        this.results = results;
    }
}
