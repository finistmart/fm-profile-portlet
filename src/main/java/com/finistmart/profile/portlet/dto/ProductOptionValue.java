package com.finistmart.profile.portlet.dto;


import java.io.Serializable;


public class ProductOptionValue implements IFieldValue, Serializable {

    private Long id;

    private ProductOption productOption;

    private ProductOptionSet productOptionSet;

    private InfoField infoField;

    private String value;

    public ProductOptionValue() {
    }


    // getters & setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductOption getProductOption() {
        return productOption;
    }

    public void setProductOption(ProductOption productOption) {
        this.productOption = productOption;
    }

    public ProductOptionSet getProductOptionSet() {
        return productOptionSet;
    }

    public void setProductOptionSet(ProductOptionSet productOptionSet) {
        this.productOptionSet = productOptionSet;
    }

    public InfoField getInfoField() {
        return infoField;
    }

    public void setInfoField(InfoField infoField) {
        this.infoField = infoField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
