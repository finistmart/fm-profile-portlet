package com.finistmart.profile.portlet.dto;


import java.io.Serializable;


public class ProductValueAdd implements Serializable {

    private Long productId;

    private Long infoFieldId;

    private String value;

    public ProductValueAdd() {
    }

    public ProductValueAdd(Long productId, Long infoFieldId, String value) {
        this.productId = productId;
        this.infoFieldId = infoFieldId;
        this.value = value;
    }

    // getters & setters


    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getInfoFieldId() {
        return infoFieldId;
    }

    public void setInfoFieldId(Long infoFieldId) {
        this.infoFieldId = infoFieldId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
