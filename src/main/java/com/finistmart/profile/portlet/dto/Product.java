package com.finistmart.profile.portlet.dto;


import java.io.Serializable;
import java.util.List;

public class Product implements Serializable {

    private Long id;

    private String name;

    private List<Category> categories;

    private List<ShowCase> showCases;

    private List<ProductFieldValue> fieldValues;

    private List<ProductOptionSet> optionSets;

    private ProductImage image;

    public Product() {
    }

    public Product(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    // getters & setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<ShowCase> getShowCases() {
        return showCases;
    }

    public void setShowCases(List<ShowCase> showCases) {
        this.showCases = showCases;
    }

    public List<ProductFieldValue> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(List<ProductFieldValue> fieldValues) {
        this.fieldValues = fieldValues;
    }

    public List<ProductOptionSet> getOptionSets() {
        return optionSets;
    }

    public void setOptionSets(List<ProductOptionSet> optionSets) {
        this.optionSets = optionSets;
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }
}
