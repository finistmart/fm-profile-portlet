package com.finistmart.profile.portlet.dto;



import java.io.Serializable;
import java.time.LocalDateTime;


public class Order implements Serializable {

    private Long id;

    private String data;

    private String status;

    private LocalDateTime created;

    private LocalDateTime updated;

    private String buyerIdent;

    private String appIdent;

    private String appType;

    public Order() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public String getBuyerIdent() {
        return buyerIdent;
    }

    public void setBuyerIdent(String buyerIdent) {
        this.buyerIdent = buyerIdent;
    }

    public String getAppIdent() {
        return appIdent;
    }

    public void setAppIdent(String appIdent) {
        this.appIdent = appIdent;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }
}
