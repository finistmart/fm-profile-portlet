package com.finistmart.profile.portlet.dto;


import java.io.Serializable;


public class ProductOptionSetAdd implements Serializable {

    private Long productOptionId;

    private Long productId;

    private String name;

    public ProductOptionSetAdd(Long productOptionId, Long productId, String name) {
        this.productOptionId = productOptionId;
        this.productId = productId;
        this.name = name;
    }

    public Long getProductOptionId() {
        return productOptionId;
    }

    public void setProductOptionId(Long productOptionId) {
        this.productOptionId = productOptionId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}