package com.finistmart.profile.portlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.profile.HttpClient;
import com.finistmart.profile.portlet.dto.Order;
import com.finistmart.profile.service.JsonMapper;
import com.finistmart.profile.service.MyOrdersService;
import com.finistmart.profile.service.TranslationsService;
import com.finistmart.util.ParamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.annotations.ActionMethod;
import javax.portlet.annotations.PortletConfiguration;
import javax.portlet.annotations.Preference;
import javax.portlet.annotations.RenderMethod;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@PortletConfiguration(
        portletName="FmMyOrdersPortlet", publicParams = {},
        prefs = {
            @Preference(name = "serviceUrl", values="http://localhost:8080/fm-service"),
            @Preference(name = "serviceLogin", values="finistmart"),
            @Preference(name = "servicePassword", values="finistmart")
        }
)
public class FmMyOrdersPortlet {
    private static final Logger logger = LoggerFactory.getLogger(FmMyOrdersPortlet.class);

    @Inject
    private PortletConfig pcfg;

    @Inject
    private MyOrdersService myOrdersService;

    @Inject
    private TranslationsService translationsService;

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    HttpClient serviceClient(PortletRequest request) {
        PortletPreferences prefs = request.getPreferences();
        String serviceUrl = prefs.getValue("serviceUrl", null);
        String serviceLogin = prefs.getValue("serviceLogin", null);
        String servicePassword = prefs.getValue("servicePassword", null);

        HttpClient client = new HttpClient();
        client.setup(serviceUrl, serviceLogin, servicePassword);
        return client;
    }

    @ActionMethod(portletName = "FmMyOrdersPortlet", actionName = "saveSettings")
    public void saveSettings(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        FmProfileApp.saveSettings(request, response);
    }

    @RenderMethod(portletNames = "FmMyOrdersPortlet", ordinal = 100, include = "/WEB-INF/jsp/myOrdersView.jsp")
    public void render(RenderRequest req, RenderResponse resp) {
        Map<String, String> translations = translationsService.getTranslationsMap(req, resp, "MyOrdersPortlet");

        PortletPreferences prefs = req.getPreferences();

        List<String> names = Collections.list(prefs.getNames());
        for (String name : names) {
            req.setAttribute(name, prefs.getValue(name, ""));
        }

        req.setAttribute("settings", names);
        List<Order> orders = new ArrayList<Order>();

        String appIdent = req.getRemoteUser();
        if (appIdent != null) {
            myOrdersService.fetchAllByAppIdent(this.serviceClient(req), null, appIdent);
        }
        try {
            req.setAttribute("ordersJson", mapper().writeValueAsString(orders));
            req.setAttribute("orders", orders);
            req.setAttribute("settingsJson", mapper().writeValueAsString(names));
            req.setAttribute("l11nJson", mapper().writeValueAsString(translations));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }



}
