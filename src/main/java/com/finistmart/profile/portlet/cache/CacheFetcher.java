package com.finistmart.profile.portlet.cache;

import org.infinispan.Cache;

public class CacheFetcher<K, E extends Object> {

    Cache<K, E> fetchCache(final String cacheName) {
        return CacheSingleton.instance.getCacheManager().getCache(cacheName);
    }
}
