package com.finistmart.profile;


import org.apache.hc.client5.http.classic.methods.HttpDelete;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpPut;
import org.apache.hc.client5.http.classic.methods.HttpUriRequest;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

public class HttpClient {

    private String serviceUrl = null;

    private String serviceLogin = null;

    private String servicePassword = null;

    public HttpClient setup(String serviceUrl, String serviceLogin, String servicePassword) {
        this.serviceUrl = serviceUrl;
        this.serviceLogin = serviceLogin;
        this.servicePassword = servicePassword;
        return this;
    }

    public String request(String requestPath, String requestType) throws IOException {
        return request(requestPath, requestType, null);
    }

    public String request(String requestPath, String requestType, String data) throws IOException {
        StringEntity entity = null;
        if (data != null) {
            entity = new StringEntity(data, Charset.forName("UTF-8"));
        }
        try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpUriRequest request = null;

            if (HttpPost.METHOD_NAME.equals(requestType)) {
                request = new HttpPost(serviceUrl + requestPath);
            } else if (HttpPut.METHOD_NAME.equals(requestType)) {
                request = new HttpPut(serviceUrl + requestPath);
            } else if (HttpDelete.METHOD_NAME.equals(requestType)) {
                request = new HttpDelete(serviceUrl + requestPath);
            } else {
                request = new HttpGet(serviceUrl + requestPath);
            }

            if (serviceLogin != null && servicePassword != null) {
                String authHash = Base64.getEncoder().encodeToString((serviceLogin + ":" + servicePassword).getBytes());
                request.setHeader("Authorization", "Basic " + authHash);
            }
            if (entity != null) {
                request.setEntity(entity);
            }
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json;charset=UTF-8");

            try (CloseableHttpResponse response = httpclient.execute(request)) {

                final HttpEntity responseEntity = response.getEntity();
                if (responseEntity != null) {
                    return EntityUtils.toString(responseEntity);
                }
                return null;

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
    }


}
