
import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonJS from 'rollup-plugin-commonjs'

export default {
  input: 'index.js',
  output: {
    globals: {
    },
    file: 'fm-profile-portlet-bundle.js',
    format: 'umd',
    name: 'window',
    extend: true,
    sourcemap: true
  },
  external: [  ],
  plugins: [
    nodeResolve({ mainFields: ['jsnext:main']}),
    commonJS({
      include: 'node_modules/**'
    }),
    babel({ runtimeHelpers: true })
  ]
};
