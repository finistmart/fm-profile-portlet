

export class FmPortletView extends HTMLElement {

    set isAdmin(isAdmin) {
        this._isAdmin = (String(isAdmin) === "true");
    }

    get isAdmin() {
        return this._isAdmin || false;
    }

    bindConfigDialog() {
        if (this.configDialogCfg) {
            let openButton = document.querySelector(this.configDialogCfg.buttonSl);
            let configDialog = document.querySelector(this.configDialogCfg.dialogSl);
            if (openButton !== null) {
                openButton.addEventListener('click', (event) => {
                    configDialog.open();
                });
            }

        }
    }

    renderTranslations() {
        if (this.translations) {
            let trTargets = this.querySelectorAll(`[class^="tr"]`);
            for (let trEl of trTargets) {
                for (let clName of trEl.classList) {
                    if (clName.startsWith('tr')) {
                        let normedClassName = clName.replace(/-/g, '.');
                        if (this.translations[normedClassName]) {
                            trEl.innerHTML = this.translations[normedClassName];
                        }
                    }
                }
            }
        }
    }

}