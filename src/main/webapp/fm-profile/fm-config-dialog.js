
import { loadFromUrl } from './node_modules/complets/prefs.js';

import { SkDialog } from ".//node_modules/skinny-widgets/src/sk-dialog.js";

export class FmConfigDialog extends SkDialog {

    async loadSettings() {
        return loadFromUrl(this.settingsUrl);
    }

    connectedCallback() {
        super.connectedCallback();
        this.loadSettings().then((settings) => {
            this.settings = settings;
        });
        this.addEventListener('save', (event) => {
            this.saveParams();
            this.close();
        });
    }

    async saveParams() {
        let dataTokens = [];
        if (! this.settings) {
            this.settings = await this.loadSettings();
        }
        for (let settingName of Object.keys(this.settings)) {
            if (settingName !== 'l11n') {
                let el = this.impl.dialog.querySelector(`[name=${settingName}]`);
                if (el != null) {
                    let value = el.value;
                    dataTokens.push(settingName + '=' + encodeURIComponent(btoa(value)));
                }
            }
        }

        let xhr = new XMLHttpRequest();
        xhr.open("POST", this.saveUrl, true);
        xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
        xhr.send(dataTokens.join('&'));

        xhr.onreadystatechange = (res) => {
            if(res.readyState == XMLHttpRequest.DONE) {
                if (res.status == 200) {
                    this.onSaved(res);
                }
            }
        }
    }

    onSaved(res) {
        console.log('onSaved', res);
    }

    get ns() {
        return this.getAttribute('ns') || '';
    }

    get saveUrl() {
        return this.getAttribute('saveUrl') || '';
    }

    get settingsUrl() {
        return this.getAttribute('settingsUrl') || '';
    }
}
