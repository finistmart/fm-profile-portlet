
import { FmPortletView } from "../fm-portlet-view.js";
import { DataSourceLocal } from "../node_modules/gridy-grid/src/datasource/data-source-local.js";
import { GridyTable } from "../node_modules/gridy-grid/src/table/gridy-table.js";
import { DateTime } from "../node_modules/dateutils/src/DateTime.js";
import { DateFormat } from "../node_modules/dateutils/src/DateFormat.js";
import { DateLocale } from "../node_modules/dateutils/src/DateLocale.js";


export class FmMyOrdersView extends FmPortletView {

    set isAdmin(isAdmin) {
        this._isAdmin = String(isAdmin) === "true"
    }

    get isAdmin() {
        return this._isAdmin || false;
    }


    render() {
        this.renderTranslations();
        if (this.isAdmin && this.isAdmin !== "false") {
            this.bindConfigDialog();
        }
    }

    connectedCallback() {
        this.render();
    }
}