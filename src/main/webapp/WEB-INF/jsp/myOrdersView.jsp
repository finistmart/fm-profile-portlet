<%@ page import="java.util.List" %>
<%@ page import="com.finistmart.profile.portlet.dto.Order" %>
<%@ page session="false" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0"  prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<portlet:defineObjects />
<portlet:actionURL var="saveSettingsURL" name="saveSettings"></portlet:actionURL>
<portlet:actionURL var="saveOrderURL" name="saveOrder"></portlet:actionURL>
<portlet:resourceURL var="getSettingsURL" id="getSettings"></portlet:resourceURL>
<portlet:renderURL var="renderURL"></portlet:renderURL>
<%
    String ctxPath = request.getContextPath();
    List<String> settings = (List<String>) renderRequest.getAttribute("settings");
    String settingsJson = (String) renderRequest.getAttribute("settingsJson");
    List<Order> orders = (List<Order>) renderRequest.getAttribute("orders");
%>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.19.8/antd.css" />
<style>
    .fm-btn-configure {
        background-image: url('<%= ctxPath %>/images/settings_black_192x192.png');
        background-size: contain;
        width: 50px;
        height: 50px;
        float: right;
        margin-top: -50px;
    }
</style>


<% if (renderRequest.isUserInRole("administrator")) { %>
    <fm-orders-config-dialog ns="${renderResponse.namespace}" id="${renderResponse.namespace}configDialog" type="confirm"
                          saveUrl="${saveSettingsURL.toString()}" settingsUrl="${getSettingsURL.toString()}" to-body>
        <h3>Settings</h3>
        <div>
            <% for (String settingName : settings) { %>
            <div>
                <label><%= settingName %></label>
                <input type="text" name="<%= settingName %>" value="<%= (String) renderRequest.getAttribute(settingName) %>" />
            </div>
            <% } %>
        </div>
        <template id="FmOrdersConfigDialogFooterTpl">
            <div>
                <button class="ant-btn btn-cancel" data-action="cancel" type="button"><span>Cancel</span></button>
                <button class="ant-btn btn-confirm ant-btn-primary" data-action="save" type="button"><span>Save</span></button>
            </div>
        </template>
    </fm-orders-config-dialog>

    <button class="fm-btn-configure" id="${renderResponse.namespace}configDialogOpenBtn"></button>
<% } %>

<sk-config
        theme="antd"
        base-path="<%= renderRequest.getContextPath() %>/fm-profile/node_modules/skinny-widgets/src"
></sk-config>

<fm-my-orders-view>
    <% if (orders != null && orders.size() > 0) { %>
    <sk-accordion>
        <% for (Order order : orders) { %>
            <sk-tab title="#<%= order.getId() %> <%= order.getCreated() %> <%= order.getStatus() %>">
                <%= order.getData() %>
            </sk-tab>
        <% } %>
    </sk-accordion>
    <% } else { %>
        <h2 class="tr-order-label-noorders">No orders</h2>
    <% } %>
</fm-my-orders-view>

<script>

    window.fmRegistry = window.fmRegistry || new Registry();

    fmRegistry.wire({
        EventBus,
        Renderer,
        ordersTr: { val: ${l11nJson} },
        ordersNs: { val: '${renderResponse.namespace}' },
        orders: { val: ${ordersJson} },
        isAdmin: { val: '<%= renderRequest.isUserInRole("administrator") ? "true" : "false" %>' },
        ordersConfigDialogCfg: { val: { buttonSl: '#<portlet:namespace/>configDialogOpenBtn', dialogSl: '#<portlet:namespace/>configDialog' }},
        SkConfig: { def: SkConfig, deps: { 'renderer': Renderer }, is: 'sk-config'},
        SkButton: { def: SkButton, deps: { 'renderer': Renderer }, is: 'sk-button'},
        SkInput: { def: SkInput, deps: { 'renderer': Renderer }, is: 'sk-input'},
        SkForm: { def: SkForm, deps: { 'renderer': Renderer }, is: 'sk-form'},
        SkAccordion: { def: SkAccordion, deps: { 'renderer': Renderer }, is: 'sk-accordion'},
        FmOrdersConfigDialog: { def: FmMyOrdersConfigDialog, deps: {}, is: 'fm-orders-config-dialog'},
        FmOrdersView: {
            def: FmMyOrdersView, deps: {
                'translations': 'ordersTr',
                'ns': 'ordersNs',
                'isAdmin': 'isAdmin',
                'configDialogCfg': 'ordersConfigDialogCfg',
                'orders': 'orders'
            }, is: 'fm-my-orders-view'
        }
    });



</script>
